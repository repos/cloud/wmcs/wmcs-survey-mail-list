WMCS Survey Mail List
=====================

Make a list of usernames suitable for surveying all Toolforge maintainers and
Cloud VPS project admins.

```
$ python3 make-toolforge-user-list.py > users.txt
$ python3 make-cloudvps-user-list.py >> users.txt
$ sort users.txt | uniq > all-users-sorted.txt
```

This list can then be used in combination with [sendBulkEmails.php][] to send
emails. See [wikitech][] for additional information.

You can also make a list of email addresses instead if desired. This has been
used in the past when using a hosted SaaS with built-in email sending to
manage the survey.

```
$ python3 make-toolforge-email-list.py > emails.txt
$ python3 make-cloudvps-email-list.py >> emails.txt
$ sort emails.txt | uniq > all-emails-sorted.txt
$ python3 make-opt-out-list.py | sort | uniq > opt-out.txt
$ grep -vxF -f opt-out.txt all-emails-sorted.txt > emails.txt
```

[sendBulkEmails.php]: https://www.mediawiki.org/wiki/Manual:SendBulkEmails.php
[wikitech]: https://wikitech.wikimedia.org/wiki/Annual_Toolforge_Survey/How_to
